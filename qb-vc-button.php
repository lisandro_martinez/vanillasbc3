<?php
/*
Plugin Name: Intuit Button
Author: rgonzalez
Version: 1.0
Description: Shortcode for vc buttom plugin.

*/

$qb_vc_btn = new QB_VC_Component_Factory();

$qb_vc_btn -> set(
    [
        "name"          => "Intuit Button",
        "shortcode"     => "qbse_btn",
        "template_file"     => dirname(__FILE__).'/templates/qb-vc-button.html',
        "params"        => // parameters, add params same as with any other content element
            [
                [
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => 'Text button',
                    'param_name'    => 'text_button',
                    'value'         => '' ,
                    'description'   => 'Enter text'                 
                ],
                [
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'URL (Link)' ),
                    'param_name'    => 'url',
                    'value'         => __( '' ),
                    'description'   => __( 'Add link to button.' )
                ],
                [
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'SiteCat' ),
                    'param_name'    => 'wa_link',
                    'value'         => __( '' ),
                    'description'   => __( 'Enter SiteCat tracking parameter.' )
                ],
                [
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Style' ),
                    'param_name'    => 'style',
                    'value'         => ["QB-orange", "QB-green", "QB-gray"],
                    'save_always'   => true,
                    'description'   => __( 'Select button style.' )
                ],
                [
                    'type'          => 'dropdown',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Button alignment' ),
                    'param_name'    => 'align',
                    'value'         => ["align-left", "align-center", "align-right" ],
                    'save_always'   => true,
                    'description'   => __( 'Select button alignment.' )
                ],
                [
                    'type' => 'dropdown',
                    'holder' => 'div',
                    'class' => '',
                    'heading' => __( 'Open link in new window?' ),
                    'param_name' => 'target',
                    'value' => array("_self", "_blank" ),
                    'description' => __( 'Select "_blank" to open the link in a new window.' )                
                ],
                [
                    'type'          => 'textfield',
                    'holder'        => 'div',
                    'class'         => '',
                    'heading'       => __( 'Extra class name' ),
                    'param_name'    => 'el_class',
                    'value'         => __( '' ),
                    'description'   => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.' )
                ],
            ]
    ]
);

$qb_vc_btn -> start();
