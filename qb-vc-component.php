<?php

/**

QB_VC_Component
This class creates a basic Visual Composer custom component

*/

class QB_VC_Component extends QB_Plugin {
 
    # $parameters have all the fields for the VC component
    private $component_name;
    private $category_name;
    private $parameters = array();
    private $extra_fields = array();
    private $is_container;
     
    /**
    
    This function creates the shortcode for WP, named: $shortcode_name and add parameters of certain type to the data
    
    The types are all VC types available like: textfield, dropdown, attach_image and textarea_html
    
    $shortcode_name: the name of the shortcode to be used in WP, i.e. [qbse-video]
    
    $parameters: array of field definitions
        example:
        [
            // defines a textfield called video_url
            [ 'type' => 'textfield', 'heading' => __( 'Youtube Video URL' ), 'param_name' => 'video_url', 'description' => __( 'Enter video URL here' ) ],
            // defines a WP image called image_id
            [ 'type' => 'attach_image', 'heading' => __( 'Image video cover' ), 'param_name' => 'image_id', 'value' => __( 'Default' ), 'description' => __( 'Choose an image' ) ]
        ]
        
    */
    
    public function set_backend( $component_name, $shortcode_name, $parameters=null, $extra_fields=null, $is_container=false, $category=null ) {

        
        $this -> is_container = $is_container;
        $this -> set_shortcode($shortcode_name);
        $this -> component_name = $component_name;
        $this -> category_name = $category?$category:"Intuit";
        
        $this -> parameters = $parameters;
        
        if (is_array($extra_fields)) {
            $this -> extra_fields = $extra_fields;            
        }
        
        # we need to add the SiteCat link if it is not present in the list of parameters
        $this->wa_link_check();
        
    }
    
    public function integrateWithVC() {

        $array_vc = array(
                "name" => $this -> component_name,
                "base" => $this -> get_shortcode(),
                "class" => "",
                "icon" => plugins_url( 'common_assets/images/qbicon.png', __FILE__ ),
                "category" => $this -> category_name,
                "params" => $this -> parameters
        ); 
        
        $array_vc = array_merge($array_vc, $this -> extra_fields);
        
        vc_map( $array_vc );

        // inherits the behaviour of the VC classes
        WPBakeryShortCodeCreator($this -> get_shortcode(), $this -> is_container);        
        
    }
    
    # let's init the whole thing
    public function init() {
        
        parent::init();
        
        # this will add our data to the VC backend
		add_action( 'vc_before_init', array($this, 'integrateWithVC' ) );
        
    }
    
    # render shortcode
	public function handle_shortcode($atts, $content = null) {

        # Pre-process the attributes
        
        # first we obtain the list of arguments using parameters as pairs and finding the right default value for each.        
        
        $atts = $this -> get_defaults($atts);
        
        # we process the content field if it is present
        $arr_content = $this -> get_content($content);
		
		# there are images parameters? then we get the raw url
		$images = $this -> get_images( $atts );

		# there are link parameters? then we get the html code
		$links = $this -> get_links( $atts );

        $atts = array_merge($atts, $arr_content, $images, $links);
        
        return parent::handle_shortcode($atts);

        
	}
    
    #####################################
    #
    # Auxiliary methods
    #
    #####################################
    
    # find the best default value option for the atts using the parameters array
    private function get_defaults($atts) {
        
        $pairs = array();
        
        foreach($this->parameters as $param) {            
            $pairs[ $param['param_name'] ] = (is_array($param['param_name']))?current($param['value']):(isset($param['value']))?$param['value']:"";
        }

        $arguments = shortcode_atts( $pairs, $atts );

        # bug fix: set the defaults for dropdowns
        if (is_array($arguments)) {
            foreach ($arguments as $ind_arg => $arg) {
                if (is_array($arg)) {
                    $arguments[$ind_arg] = current($arg);
                }
            }
        }
        
        return $arguments;

    }
    
    # process the content field if it is present
    private function get_content($content) {
        
        $arr_content = array();
        if ($content) {
            $arr_content["content"] = do_shortcode($content);
        }
        return $arr_content;
        
    }
    
    # find the images of all arguments, if any
    private function get_images($arguments) {
        
        $images = array();
        
        foreach($this->parameters as $param) {
            
            if ($param['type'] == 'attach_image') {

                $raw_image = new WP_Query( array( 'post_type' => 'attachment', 'attachment_id' => $arguments[$param["param_name"]] ));

                // Let's get the URL from that image 
                if(isset($raw_image->posts[0])) {
                    $images[ $param['param_name'] ] = $raw_image->posts[0]->guid;            
                } else {
                    $images[ $param['param_name'] ] = null;
                }

            }
            
        }
        
        return $images;
        
    }

    # find the links of all arguments, if any
    private function get_links($arguments) {
        
        $links = array();
        
        foreach($arguments as $ind=>$arg) {
            
            # is it a url formatted url? (vc_link field)
            if (substr($arg, 0, 4)=='url:' AND strpos($arg, '|')!==false) {
                
                // Let's get the HTML 
                $arr_arg = explode('|', urldecode($arg));

		$url = substr($arr_arg[0], 4);

		$title = substr($arr_arg[1], 6);
		$title = ($title)?$title:$url;

		$target = trim(substr($arr_arg[2], 7));

		$links[$ind]='<a href="'.$url.'" target="'.$target.'">'.$title.'</a>';

            }
            
        }
        
        return $links;
        
    }

    // add wa_link check if not present in the parameters
    private function wa_link_check() {
        $present=false;
        if (is_array($this->parameters)) {
            foreach($this->parameters as $item) {
                if ($item["param_name"]=='wa_link') {
                    $present=true;
                }
            }
        }
        if (!$present) {            
            $this -> parameters[] = array(
                'type' => 'textfield',
                'holder' => 'div',
                'class' => '',
                'heading' => __( 'SiteCat' ),
                'param_name' => 'wa_link',
                'value' => __( '' ),
                'description' => __( 'Enter SiteCat tracking param' )
            );
        }
    }
    
}

// Global function for class creation
function WPBakeryShortCodeCreator($shortcode, $is_container) {
    
    if ($is_container) {
        $extend_class_name = 'WPBakeryShortCodesContainer';
    } else {
        $extend_class_name = 'WPBakeryShortCode';
    }
    
    if ( class_exists( $extend_class_name ) ) {
        
        // changes: asdf_qwer_zxcv to: Asdf_Qwer_Zxcv
        $arr_shortcode_parts = explode("_", $shortcode);
        $shortcode_camelcased=null;
        foreach($arr_shortcode_parts as $asp) {
            $shortcode_camelcased .= ($shortcode_camelcased)?"_":"";
            $shortcode_camelcased .= ucwords($asp);
        }
        
        $class_name = "WPBakeryShortCode_".ucwords($shortcode_camelcased);
        
        $php = <<<PHP_CODE
        class $class_name extends $extend_class_name {}
PHP_CODE;
        
        eval($php);
        
    }
}

