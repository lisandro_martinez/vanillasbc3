<?php

/**

QB_VC_Component_Factory
This class allows to create nested components (containers with children) in Visual Composer
Needs: QB_VC_Component class
Usage

# create the object
$vc_pricing_table = new QB_VC_Component_Factory();

# set the parameters
$vc_pricing_table -> set(
    array(
        "name" => "Intuit Pricing Table",					// name of the component
        "shortcode" => "princing_table",					// nameof the shortcode
		"const"			=> [
			"container_div_id"	=> "uuid"					// add a constant with this pair of name => value
		],
		"is_container"	=> false,							// true: force the element to be a container
		"except"		=> "shortcode1, shortcode2",		// allow all children, except the ones specified here (shortcode names comma separated)
		"keep_siblings" => true, 							// this will allow to add all the VC elements and not only the children defined on it
		"js"	=>	[										// add JS to WP queue
						["name"=>"qb-vc-cc-script", 
						 "path" => "qb-vc-conditional-container/script.js", 
						 "dependencies" => ['jquery']		// dependendies can be defined here
						]
					],
		"css"	=>	[										// add CSS to WP queue
						["name"=>"qb-vc-cc-css", 
						 "path" => "qb-vc-cc/style.css"
						]
					],
		"render_hook" 	=> "render_condition",				// allows to have a custom render function that will override regular mustache rendering
        "params" => [                                       // add params same as with any other content element
                        [
                            "type" => "textfield",
                            "heading" => __("Header title"),
                            "param_name" => "heading_title",
                            "description" => __("Enter a Title")
                        ],
                    ],
        "template_file"=>dirname(__FILE__).'/templates/table-template.php', // the mustache template path
        "children" =>                                                       // define as many levels/children as needed with the same parameters
            [
                [
                    "name" => "Intuit Pricing Table Column",
                    "shortcode" => "princing_row",
                    "template_file" => ...
                    "params" => ...
                    "children" => ...

# start the experience
$vc_pricing_table -> start();

Help:
    https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=524362
    https://gist.github.com/Webcreations907/ff0b068c5c364f63e45d

*/

class QB_VC_Component_Factory {

    # this is an array holding the definitions of the parameters to be used in each level of the composer
    private $maps;

    # set the map in the object
    public function set($maps) {
        $this->maps=$maps;
    }
    
    # starts the recursive definitions of all levels
    public function start() {
        # from now on the class will be used statically
        QB_VC_Component_Factory::recursive($this->maps);
    }

    # definition of each level of the VC components
    static public function recursive($maps, $is_root_level=true, $parent_shortcode=null) {
        
        if (is_array($maps) and array_key_exists("name", $maps) and array_key_exists("shortcode", $maps)) {

            # flag to identify a container
            if (array_key_exists("is_container", $maps)) {
				$is_container = $maps["is_container"];
			} else {
				$is_container = false;
			}
			
            # default parameter of the VC element
            $extra_fields["content_element"] = true;
            
            # this level has parameters on it? then it should show those fields
            if (array_key_exists("params", $maps)) { 
                $extra_fields["show_settings_on_create"] = true;
            } else {
                $extra_fields["show_settings_on_create"] = false;
                $maps["params"]=null;                
            }
            
            # if it has a parent shortcode it means that is acting "as child"
            if ($parent_shortcode) {
                $extra_fields["as_child"] = array('only' => $parent_shortcode);
            }

            # it has children?, if true the create a field: "as_parent" => array('only' => 'test3_1,test3_2') 
            $children = null; 
            if (array_key_exists("children", $maps) and (is_array($maps["children"]) or $maps["children"]=="all")) {
				if (is_array($maps["children"])) {
					foreach($maps["children"] as $child) {
						$children .= ($children)?",":"";
						$children .= $child["shortcode"];
					}
					# should we delete all siblings? if this condition is true it will remove all VC elements from the container, except the children specified
					if (!array_key_exists("keep_siblings", $maps) or !$maps["keep_siblings"]) { 
						$extra_fields["as_parent"] = array('only' => $children); // $children is: child1,child2
					}
				}
                $is_container = true;
            }

			# if except is defined it overwrites previous children settings
            if (array_key_exists("except", $maps)) {
				$extra_fields["as_parent"] = array('except' => $maps["except"]);
                $is_container = true;
            }

            # the first container must have the proper JS view (inherited by VC)
            if ($is_container) {
                $extra_fields["is_container"] = true;
                if ($is_root_level) $extra_fields["js_view"] = 'VcColumnView';
            }
            
            # Create the Visual Component object
            $component = new QB_VC_Component;
            $component -> set_backend($maps["name"], $maps["shortcode"], $maps["params"], $extra_fields, $is_container, $maps["category"]);

            # do we have a template?
            if (array_key_exists("template_file", $maps)) {
				# do we need to add constants to the template?
				if (array_key_exists("const", $maps)) {
					$cons = $maps["const"];
				} else {
					$cons = [];
				}
                $component -> set_frontend(null, ['template' => $maps["template_file"], 'type' => 'mustache'], $cons);
            }

            # do we have js files?
            if (array_key_exists("js", $maps)) {
				foreach($maps["js"] as $js)
					$component->add_js($js);
			}
			
            # do we have css files?
            if (array_key_exists("css", $maps)) {
				foreach($maps["css"] as $css)
					$component->add_css($css);
			}
			
			# does the component his own rendering function?
            if (array_key_exists("render_hook", $maps)) {
				$component -> set_render_hook($maps["render_hook"]);
			}

            # let's start
            $component -> init();

            # has children?
            if (array_key_exists("children", $maps) and is_array($maps["children"])) {
                # be fruitful and multiply
                foreach($maps["children"] as $child) {
                    QB_VC_Component_Factory::recursive($child, false, $maps["shortcode"]);
                }
            }
            
        }
        
    }
    
}

