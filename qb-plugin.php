<?php

/**

QB_Plugin
This class register all the JS/CSS files needed for the component
and will include them only if the shortcode is present in the page

$shortcode_name = "hello-world";

Example: 
* 
$hello_world = 	new QB_Plugin();
$hello_world ->	set_shortcode($shortcode_name);
$hello_world ->	set_frontend(
					[
						// defines styles
						[	
							'path' => 'css/hello_world.css',
							'name' => 'hello-world-css'
							
						],
						// defines javascript
						[
							'path' => 'js/hello_world.js',
							'name' => 'hello-world-js',
							'dependencies' => ['jquery']
						]
					], 
					['template'=>'<p class="my-hello-world">Hello World!</p><p>Lorem ipsum at dolor at simet.</p>']);
$hello_world ->	init();

*/

//require_once( get_template_directory()."/resources/admin/application.php" );

class QB_Plugin {
    
    # $add_script: variable used to control if the shortcode is present
    # $shortcode_name is the... WP shortcode name
    # $css_files and $js_files will have all the assets to be loaded
    # all the paths to assets must be relative to the current plugin directory
    private $add_script, $shortcode_name, $css_files = array(), $js_files = array();
    
    # $template: the mustache template
    # $cons additional values to pass as parameters to the mustache template
    protected $template, $cons, $render_hook;

    # set the shortcode name
    public function set_shortcode($shortcode_name) {
        $this -> shortcode_name = $shortcode_name;
    }
    
    # get the shortcode name
    protected function get_shortcode() {
        return $this -> shortcode_name;
    }
    
    # the followinf functions adds css/js files to he plugin list of assets to be used in the WP queue

    public function add_css($css) {

		array_push($this->css_files, $css);

		# need to enqueue this always in the framework to work with transient content
		wp_enqueue_style($css['name'], plugins_url( $css['path'], __FILE__ ), (array_key_exists('dependencies', $css))?$css['dependencies']:null);

		# keep the record of loaded assets
		self::record_assets($this->css_files, 'css');

    }
    
    public function add_js($js) {

        array_push($this->js_files, $js);

		# need to enqueue this always in the framework to work with transient content
		wp_register_script($js['name'], plugins_url( $js['path'], __FILE__ ), (array_key_exists('dependencies', $js))?$js['dependencies']:null);

        # keep the record of loaded assets
		self::record_assets($this->js_files, 'js');

    }
    
    # setup hooks/actions
    # $shortcode_class is the class of the child class that is the one that WP needs to call.
    # optionally a inferior class can handle his own shortcode.... thats why we have a flag
    public function init() {
    
        add_action('init', array($this, 'register_script'));
        add_action('wp_footer', array($this, 'print_script'));
        add_shortcode($this -> shortcode_name, array($this, 'handle_shortcode'));            
        
    }
    
    # render shortcode
    public function handle_shortcode($atts) {
		
        # here we load the styles defined in the $css_files
        # and we try to do it only once
/*
        if ( ! $this -> add_script )    {
            if (is_array($this -> css_files)) {
                foreach($this -> css_files as $index => $css) {
                    # TODO: we need to manage dependencies here
                    wp_enqueue_style($css['name'], plugins_url( $css['path'], __FILE__ ), (array_key_exists('dependencies', $css))?$css['dependencies']:null);
                }
            }
        }
*/
        # true means that the shortcode is present
        $this -> add_script = true;

		if ($this->cons) {
			$atts = array_merge($atts, $this->process_cons($this->cons));
		}

		# is there a custom render function?
		
		if ($this->render_hook) {
			$rh = $this->render_hook;
			return $rh($atts);
		} else {
            if (class_exists('Mustache_Engine')) {
				$m = new Mustache_Engine;
				return $m->render($this -> template, $atts);
			} else {
				return "Mustache not found";
			}
		}

    }
 
    /**
    
    This function load all the assets and render the template using the correspondent engine
    
    $assets: array of js and css paths used to be properly load using wp native functions

    $template: the template file to be rendered using mustache
            
    */
    
    public function set_frontend( $assets=null, $template=null, $cons=null) {

        $this -> cons = $cons;
        $this -> template   = 	(	array_key_exists('template', $template) and
									$template['template'] and 
									file_exists($template['template']) 
								) ? 
								file_get_contents($template['template']):
								$template['template'];


        if ($assets!=null){
            foreach ($assets as $index => $asset) {

                if (strpos($asset['path'], '.css') !== false){
                    $this->add_css($asset);
                }else{
                    $this->add_js($asset);
                }
            }
        }

    }
     
    # register all scripts
    public function register_script() {

        # scripts -- loaded in footer
        if (is_array($this -> js_files)) {
            foreach($this -> js_files as $index => $js) {
                wp_register_script($js['name'], plugins_url( $js['path'], __FILE__ ), (array_key_exists('dependencies', $js))?$js['dependencies']:null);
            }
        }
        
    }

    # in footer
    public function print_script() {
    
        # if ( ! $this -> add_script ) return;
        
        # scripts -- loaded in footer
        if (is_array($this -> js_files)) {
            foreach($this -> js_files as $index => $js) {
                wp_print_scripts($js['name'], plugins_url( $js['path'], __FILE__ ), $js['dependencies']);
            }
        }
        
    }
    
    // generate random unique id if needed
	private function process_cons($cons) {
		$ret = [];
		if (is_array($cons)) {
			foreach($cons as $i=>$c) {
				if ($c=="uuid") { // this special flag is for generating unique random id values
					$ret[$i]=uniqid().rand(1,1000000);
				} else {
					$ret[$i]=$c;
				}
			}
		}
		return $ret;
	}
	
	# set a function call to do the render (optional)
	public function set_render_hook($hook) {
		$this->render_hook = $hook;
	}

	# keep record of loaded assets
	public static function record_assets($files, $type) {
		global $qb_plugin_assets;
		foreach($files as $file) {
			$qb_plugin_assets[$type][] = un_get_root_domain(plugins_url( $file["path"], __FILE__ ));
		}
	}
	
	# return all assets from all loaded components
	public static function get_assets() {
		global $qb_plugin_assets;
		return ['css'=>array_values(array_unique($qb_plugin_assets['css'])), 'js'=>array_values(array_unique($qb_plugin_assets['js']))];
	}
    
}
